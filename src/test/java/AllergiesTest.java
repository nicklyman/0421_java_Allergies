import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;

public class AllergiesTest {

  @Test
  public void runAllergyScore_128_cats() {
    Allergies testAllergies = new Allergies();
    String expected = "cats";
    String returnValue = testAllergies.runAllergyScore(128).get(0);
    assertEquals(expected, returnValue);
  }
  @Test
  public void runAllergyScore_64_pollen() {
    Allergies testAllergies = new Allergies();
    String expected = "pollen";
    String returnValue = testAllergies.runAllergyScore(64).get(0);
    assertEquals(expected, returnValue);
  }
  @Test
  public void runAllergyScore_200_cats() {
    Allergies testAllergies = new Allergies();
    String expected = "cats";
    String returnValue = testAllergies.runAllergyScore(200).get(0);
    assertEquals(expected, returnValue);
  }
  @Test
  public void runAllergyScore_200_pollen() {
    Allergies testAllergies = new Allergies();
    String expected = "pollen";
    String returnValue = testAllergies.runAllergyScore(200).get(1);
    assertEquals(expected, returnValue);
  }
  @Test
  public void runAllergyScore_200_strawberries() {
    Allergies testAllergies = new Allergies();
    String expected = "strawberries";
    String returnValue = testAllergies.runAllergyScore(200).get(2);
    assertEquals(expected, returnValue);
  }
}
