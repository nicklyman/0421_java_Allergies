import java.util.ArrayList;

public class Allergies {

  public ArrayList<String> runAllergyScore(Integer score) {
    ArrayList<String> outputArray = new ArrayList<String>();

      if (score == 0) {
      outputArray.add("You have no allergies!");
      return outputArray;
    } if (score >= 128) {
      outputArray.add("cats");
      score -= 128;
    } if (score >= 64) {
      outputArray.add("pollen");
      score -= 64;
    } if (score >= 32) {
      outputArray.add("chocolate");
      score -= 32;
    } if (score >= 16) {
      outputArray.add("tomatoes");
      score -= 16;
    } if (score >= 8) {
      outputArray.add("strawberries");
      score -= 8;
    } if (score >= 4) {
      outputArray.add("shellfish");
      score -= 4;
    } if (score >= 2) {
      outputArray.add("peanuts");
      score -= 2;
    } if (score >= 1) {
      outputArray.add("eggs");
      score -= 1;
    } if (score != 0) {
      outputArray.add("Input number not in range.");
    } return outputArray;
  }
}
